<?php

use Illuminate\Database\Eloquent\ModelNotFoundException;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('home');
});

Route::get('/textpalace/about', function()
{
	return View::make('about');
});

Route::get('users', function () 
{
	$users = User::all();
	return View::make('users.index')->withUsers($users);
});


Route::get('users/{id}', function ($id)
{
	$user = User::findOrFail($id);

	return View::make('users.show')->withUser($user);

})->where('id', '[0-9]+');


Route::get('users/{username}', array( 
	'as' => 'users.username', 
	function ($username)
	{
		$user = User::where('username', $username)->firstOrFail();
		
		return View::make('users.show')->withUser($user);
	}
))->where('username', '[0-9A-Za-z\-]+');


Route::resource('entries', 'EntryController');
// *** ERRORS ***

App::error(function(ModelNotFoundException $e)
{
    return Response::view('errors.missing', array(), 404);
});

App::missing(function($exception)
{
    return Response::view('errors.missing', array(), 404);
});



