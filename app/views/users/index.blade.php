@extends('layouts.master')

@section('content')
	<ul>
		@foreach($users as $user)
			<li>{{ link_to_route(
					'users.username', 
					$user->username, 
					array('username' => $user->username) 
				) }}</li>
		@endforeach
	</ul>
@stop