@extends('layouts.master')

@section('content')

	<div class="container">
		<h1>{{ $user->username}}</h1>
		
		<p>{{ Gravatar::image($user->email) }}</p>
		
		<p>{{{ $user->bio }}}</p> {{-- this echos and escapes html --}}
		
		<p>{{ $user->bio }}</p> {{-- this echos and does not escape --}}

		<p>Member since {{ date('j F Y', time($user->created_at)) }}
	</div>

@stop