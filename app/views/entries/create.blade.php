@extends ('layouts.master')

@section('content')
	
	<div class="container">
		<h1>Create Entry</h1>

	<div class="form-group">
		{{ Form::open(array('route' => 'entries.store')) }}
	</div>
			{{ Form::label('title', 'Title')}}
			{{ Form::text('title', NULL, array('placeholder' => 'Yurr text Hurr pe Durr')) }}
	<div class="form-group">
			{{ Form::label('text', 'Text') }}
			{{ Form::textarea('text', NULL, array('placeholder', "Yurr text Hurr pe Durr")) }}
	</div>

	<div class="form-group">
		{{ Form::submit('Create', array(
			'class' => 'btn btn-primary')) }}
	</div>

		{{ Form::close() }}
@stop