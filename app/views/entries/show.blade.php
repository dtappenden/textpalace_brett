@extends ('layouts.master ')

@section('content')
	
	<div class="container">
		<h1>{{ $entry->title }}</h1>

		<p>{{ $entry->text }}</p>

		<p>Created {{ date('jS F Y, time($entry->created_at') }}</p>

	</div>

@stop