@extends('layouts.master')

@section('content')
	
	<div class="contianer">
		<h1>Text Entries</h1>
		<ul>
			@foreach($entries as $entry)
				<li>{{ link_to_route(
					'entries.show',
					$entry->id . " - " . $entry->title,
					array('id' => $entry->id)
					) }}</li>
			@endforeach
		</ul>
	</div>
@stop