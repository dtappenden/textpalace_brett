<?php

class EntriesTableSeeder extends Seeder {
	
	public function run ()
		{
			DB::table('entries')->truncate();

			$faker = Faker\Factory::create();

			for( $i = 0; $i <100; $i += 1) {

			Entry::create( [
				'title' => $faker->sentence(rand(3, 6)),
				'text' => implode("\n\n", $faker->paragraphs(rand(2,4)))
			]);
		}
	}
}